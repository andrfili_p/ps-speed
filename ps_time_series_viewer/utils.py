# -*- coding: utf-8 -*-

"""
/***************************************************************************
Name                : PS Time Series Viewer
Description         : Computation and visualization of time series of speed for
                    Permanent Scatterers derived from satellite interferometry
Date                : Jul 25, 2012
copyright           : (C) 2012 by Giuseppe Sucameli and Matteo Ghetta (Faunalia)
email               : brush.tyler@gmail.com matteo.ghetta@faunalia.it

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.PyQt.QtCore import QRegExp, Qt, QDate, pyqtSignal
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtWidgets import QMessageBox, QInputDialog

from qgis.core import (
    QgsDataSourceUri,
    QgsMessageLog,
    QgsFeatureRequest,
    QgsVectorLayer,
    QgsWkbTypes,
    QgsSettings,
    QgsPointXY,
    QgsRectangle
)

from qgis.gui import (
    QgsMapToolIdentifyFeature,
    QgsMapToolEmitPoint,
    QgsMapTool,
    QgsRubberBand
)

from qgis.utils import iface

import psycopg2
from processing.algs.gdal.GdalUtils import GdalUtils
from .ui.data_structure_dialog import DataStructureType


def getXYvalues(ps_layer, feature):

    layer = ps_layer
    fid = feature.id()
    attrs = feature.attributes()

    x, y = [], []	# lists containg x,y values
    infoFields = {}	# hold the index->name of the fields containing info to be displayed

    ps_source = ps_layer.source()
    ps_fields = ps_layer.fields()

    providerType = ps_layer.providerType()
    uri = ps_source
    subset = ""

    # Shapefile, geopackage, etc
    if providerType in ('ogr', 'delimitedtext'):
        for idx, fld in enumerate(ps_fields):
            if QRegExp( "^\d{8}$", Qt.CaseInsensitive ).indexIn( fld.name() ) < 0:
                # info fields are all except those containing dates
                infoFields[ idx ] = fld
            else:
                x.append( QDate.fromString( fld.name(), "yyyyMMdd" ).toPyDate() )
                y.append( float(attrs[ idx ]) )

    elif providerType in ['postgres', 'spatialite']:# either PostGIS or SpatiaLite

        # check the structure of the data
        structure = _checkStructure(ps_layer)

        if not structure:
            return

        if structure == 'external':

            # fields containing values
            dateField = "dataripresa"
            valueField = "valore"
            infoFields = dict(enumerate(ps_fields))

            # search for the id_dataset and code_target fields needed to join
            # PS and TS tables
            code = None
            for idx, fld in enumerate( ps_fields ):
                if fld.name().lower() == "code":
                    code = attrs[ idx ]
                if fld.name().lower() == 'cod_passaggio':
                    cod_passaggio = attrs[ idx ]

            if code is None:
                QgsMessageLog.logMessage( "code is None. Exiting" % code, "PSTimeSeriesViewer" )
                return

            subset = "code='{}' AND cod_passaggio={}".format(code, cod_passaggio)

            # create the uri
            dsuri = QgsDataSourceUri( ps_layer.source() )
            default_tbl_name = f"ts_{dsuri.table()}"
            ts_tablename = _askTStablename( ps_layer,  default_tbl_name )
            dsuri.setDataSource( dsuri.schema(), ts_tablename, None ) # None or "" ? check during tests
            dsuri.setWkbType(QgsWkbTypes.Unknown)
            dsuri.setSrid(None)
            uri = dsuri.uri()

            # load the layer containing time series
            ts_layer = _createTSlayer( uri, providerType, subset )
            if ts_layer is None:
                return


            # get time series X and Y values
            try:
                x, y = _getXYvalues( ts_layer, dateField, valueField )
            except:
                QMessageBox.warning( iface.mainWindow(),
                        "PS Time Series Viewer",
                        "No time series values found for the selected point." )
                QgsMessageLog.logMessage( f"provider: {providerType} - uri: {uri}\nsubset: {subset}", "PSTimeSeriesViewer" )
            finally:
                # ts_layer.deleteLater()
                # del ts_layer
                layer = ts_layer

            if len(x) * len(y) <= 0:
                QMessageBox.warning( iface.mainWindow(),
                        "PS Time Series Viewer",
                        "No time series values found for the selected point." )
                QgsMessageLog.logMessage( f"provider: {providerType} - uri: {uri}\nsubset: {subset}", "PSTimeSeriesViewer" )
                return


        elif structure == 'internal':

            x, y = [], []

            for idx, fld in enumerate(ps_fields):
                if QRegExp( "^\d{8}$", Qt.CaseInsensitive ).indexIn( fld.name() ) < 0:
                # if QRegExp( "D\\d{8}", Qt.CaseInsensitive ).indexIn( fld.name() ) < 0:
                    # info fields are all except those containing dates
                    infoFields[ idx ] = fld
                else:
                    # x.append( QDate.fromString( fld.name()[1:], "yyyyMMdd" ).toPyDate() )
                    x.append( QDate.fromString( fld.name(), "yyyyMMdd" ).toPyDate() )
                    y.append( float(attrs[ idx ]) )

    return x, y, infoFields, fid, layer

def _checkStructure(ps_layer):
    # utility to check the data structure in a PG database (internal or external)

    # get URI from layer source
    dsuri = QgsDataSourceUri( ps_layer.source() )
    ps_layer_uri = dsuri.uri()
    ps_layer_uri = ps_layer_uri.replace("'", "")

    # if connection with service
    if 'service' in ps_layer_uri:
        for param in dsuri.uri().split(" "):
            if param.startswith('service'):
                conn = param.split("=")[1]

                myConnection = psycopg2.connect(service=conn)

    # if connection with user and pws
    else:
        param_dict = {}
        ps_layer_connection_info = GdalUtils.ogrConnectionStringFromLayer(ps_layer)
        ps_layer_connection_info = ps_layer_connection_info.replace("'", "")

        for i in ps_layer_connection_info.split(" "):
            if 'dbname' in i:
                param_dict['dbname'] = i.split("=")[1]
            if 'host' in i:
                param_dict['host'] = i.split("=")[1]
            if 'user' in i:
                param_dict['user'] = i.split("=")[1]
            if 'password' in i:
                param_dict['password'] = i.split("=")[1]
            if 'port' in i:
                param_dict['port'] = i.split("=")[1]
        param_dict['schema'] = dsuri.schema()

        myConnection = psycopg2.connect(
            database=param_dict['dbname'],
            user=param_dict['user'],
            password=param_dict['password'],
            host=param_dict['host'],
            port=param_dict['port']
        )

    # start connection, cursor and get a list of available tables in schema
    with myConnection as mycon:

        try:

            cur = myConnection.cursor()
            query = '''
                SELECT structure FROM public.settings
                WHERE name = 'public.{}'
            '''.format(dsuri.table())
            cur.execute(query)
            results = cur.fetchall()

            # get the structure as a string
            structure = results[0][0]

        except:
            structure = None

    if not structure:
        data_structure_dialog = DataStructureType(iface)
        data_structure_dialog.exec_()
        structure = data_structure_dialog.writeTable(ps_layer)

    return structure

def _getXYvalues(ts_layer, dateField, valueField):
        # utility function used to get the X and Y values

        x, y = [], []

        # get indexes of date (x) and value (y) fields
        dateIdx, valueIdx = None, None
        for idx, fld in enumerate(ts_layer.dataProvider().fields()):
            if fld.name().lower() == dateField:
                dateIdx = idx
            elif fld.name().lower() == valueField:
                valueIdx = idx

        if dateIdx is None or valueIdx is None:
            QgsMessageLog.logMessage(f"field {dateField} -> index {dateIdx}, field {valueField} -> index {valueIdx}. Exiting", "PSTimeSeriesViewer")
            return


        # fetch and loop through all the features
        request = QgsFeatureRequest()
        request.setSubsetOfAttributes([dateIdx, valueIdx])
        for f in ts_layer.getFeatures( request ):
            # get x and y values
            a = f.attributes()
            x.append( QDate.fromString( str(a[ dateIdx ]), "yyyyMMdd" ).toPyDate() )
            y.append( float(a[ valueIdx ]) )


        return x, y

def _askTStablename(ps_layer, default_tblname=None):
    # utility function used to ask to the user the name of the table
    # containing time series data

    # call the function that returns the list of available ts_ layers
    rlist = _getTsTables(ps_layer)

    structure = _checkStructure(ps_layer)

    settings = QgsSettings()
    if not settings.value(f'ps-speed/{ps_layer.id()}'):

        if structure == 'external':

            if default_tblname is None:
                default_tblname = ""

            # ask a tablename to the user
            if ps_layer.id() or not ts_tablename:
                tblname, ok = QInputDialog.getItem(
                    iface.mainWindow(),
                    "PS Time Series Viewer",
                    "Insert the name of the table containing time-series",
                    rlist
                        )
                settings.setValue(f'ps-speed/{ps_layer.id()}', tblname)
                if not ok:
                    return False

                ts_tablename = tblname
    else:
        ts_tablename = settings.value(f'ps-speed/{ps_layer.id()}')

        return ts_tablename


def _createTSlayer(uri, providerType, subset=None):
    # utility function used to create the vector layer containing time
    # series data
    layer = QgsVectorLayer( uri, "time_series_layer", providerType )
    if not layer.isValid():
        QMessageBox.warning( iface.mainWindow(),
                "PS Time Series Viewer",
                f"The layer {ts_tablename} wasn't found.")
        ts_tablename = None
        return

    if subset is not None:
        layer.setSubsetString( subset )

    return layer


def _getTsTables(ps_layer):
    # given a layer checks the connection settings and returns a list of all
    # the tables with name starting with ts_

    # get URI from layer source
    dsuri = QgsDataSourceUri( ps_layer.source() )
    ps_layer_uri = dsuri.uri()
    ps_layer_uri = ps_layer_uri.replace("'", "")

    # if connection with service
    if 'service' in ps_layer_uri:
        for param in dsuri.uri().split(" "):
            if param.startswith('service'):
                conn = param.split("=")[1]

                myConnection = psycopg2.connect(service=conn)

    # if connection with user and pws
    else:
        param_dict = {}
        ps_layer_connection_info = GdalUtils.ogrConnectionStringFromLayer(ps_layer)
        ps_layer_connection_info = ps_layer_connection_info.replace("'", "")

        for i in ps_layer_connection_info.split(" "):
            if 'dbname' in i:
                param_dict['dbname'] = i.split("=")[1]
            if 'host' in i:
                param_dict['host'] = i.split("=")[1]
            if 'user' in i:
                param_dict['user'] = i.split("=")[1]
            if 'password' in i:
                param_dict['password'] = i.split("=")[1]
            if 'port' in i:
                param_dict['port'] = i.split("=")[1]
        param_dict['schema'] = dsuri.schema()

        myConnection = psycopg2.connect(
            database=param_dict['dbname'],
            user=param_dict['user'],
            password=param_dict['password'],
            host=param_dict['host'],
            port=param_dict['port']
        )

    # start connection, cursor and get a list of available tables in schema
    # startingwith ts_
    with myConnection as mycon:

        cur = myConnection.cursor()
        query = '''
            select table_name from information_schema.tables
            where table_schema in ('{}')
            and
            table_name ILIKE 'ts%'
        '''.format(param_dict['schema'])

        cur.execute(query)
        results = cur.fetchall()

    # convert the results from a list of tuple to a list of items
    rlist = [i[0] for i in results]

    return rlist

class CustomRectangleMapTool(QgsMapToolIdentifyFeature, QgsMapToolEmitPoint):
    rectangleCreated = pyqtSignal()
    deactivated = pyqtSignal()
    canvasClicked = pyqtSignal(list)

    def __init__(self, canvas, layer):
        self.canvas = canvas
        self.layer = layer
        QgsMapToolIdentifyFeature.__init__(self, canvas, self.layer)
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rubberBand = QgsRubberBand(self.canvas, QgsWkbTypes.GeometryType.PolygonGeometry)
        self.rubberBand.setColor(QColor(255, 0, 0, 100))
        self.rubberBand.setWidth(2)

        # clear the map from the rubberband
        self.reset()

    def reset(self):
        """
        some standard method and variable to clear the map and the rubberband
        """
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBand.reset(QgsWkbTypes.GeometryType.PolygonGeometry)

    def canvasPressEvent(self, e):
        """
        the click event. Convert the mouse click to map canvas coordinates
        """

        self.clicked_point = self.toLayerCoordinates(self.layer, e.mapPoint())
        features_identified = self.identify(e.x(), e.y())

        features = []
        for feature in features_identified:
            features.append(feature.mFeature)
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True

        self.canvasClicked.emit(features)

        # call the method to show the rectangle on the map
        self.showRect(self.startPoint, self.endPoint)

    def canvasReleaseEvent(self, e):
        """
        the canvas release event (final click on the map)
        """
        request = QgsFeatureRequest()

        self.isEmittingPoint = False
        if self.rectangle() is not None:
            self.rectangleCreated.emit()

            rectangle = self.toLayerCoordinates(self.layer, self.rectangle())
            request.setFilterRect(rectangle)

            features = [feature for feature in self.layer.getFeatures(request)]

            self.canvasClicked.emit(features)

        # clean the map from the rectangle
        self.reset()

    def canvasMoveEvent(self, e):
        """
        the move event, when the mouse is moved around the map
        """
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showRect(self.startPoint, self.endPoint)

    def showRect(self, startPoint, endPoint):
        """
        fill the rubberband polygon as a rectangle from the clicked points
        """
        self.rubberBand.reset(QgsWkbTypes.GeometryType.PolygonGeometry)
        if startPoint:
            if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
                return

            point1 = QgsPointXY(startPoint.x(), startPoint.y())
            point2 = QgsPointXY(startPoint.x(), endPoint.y())
            point3 = QgsPointXY(endPoint.x(), endPoint.y())
            point4 = QgsPointXY(endPoint.x(), startPoint.y())

            self.rubberBand.addPoint(point1, False)
            self.rubberBand.addPoint(point2, False)
            self.rubberBand.addPoint(point3, False)
            # True to update canvas
            self.rubberBand.addPoint(point4, True)
            self.rubberBand.show()

    def rectangle(self):
        """
        return the rectangle from the rubberband
        """
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None

        return QgsRectangle(self.startPoint, self.endPoint)

    def setRectangle(self, rect):
        if rect == self.rectangle():
            return False

        if rect is None:
            self.reset()
        else:
            self.startPoint = QgsPointXY(rect.xMaximum(), rect.yMaximum())
            self.endPoint = QgsPointXY(rect.xMinimum(), rect.yMinimum())
            self.showRect(self.startPoint, self.endPoint)
        return True

    def deactivate(self):
        QgsMapTool.deactivate(self)
        self.deactivated.emit()
