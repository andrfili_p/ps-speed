# -*- coding: utf-8 -*-

"""
/***************************************************************************
Name                : PS Time Series Viewer
Description         : Computation and visualization of time series of speed for
                    Permanent Scatterers derived from satellite interferometry
Date                : Jul 25, 2012
copyright           : (C) 2012 by Giuseppe Sucameli (Faunalia)
email               : brush.tyler@gmail.com

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.PyQt.QtCore import Qt, QRegExp, QDate, QFileInfo, QDir
from qgis.PyQt.QtGui import QIcon, QCursor
from qgis.PyQt.QtWidgets import (
    QAction,
    QInputDialog,
    QMessageBox,
    QApplication,
    QComboBox
)

from qgis.core import (
    QgsMapLayer,
    QgsWkbTypes,
    QgsFeature,
    QgsFeatureRequest,
    QgsMessageLog,
    QgsDataSourceUri,
    QgsVectorLayer
)

from qgis.gui import QgsMapToolIdentifyFeature
from processing.algs.gdal.GdalUtils import GdalUtils

import psycopg2
import os
from functools import partial

from .utils import getXYvalues
from .ui.create_plot_dialog import createPlotDialog



class PSTimeSeries_Plugin:

    def __init__(self, iface):
        self.iface = iface
        self.featFinder = None
        self.running = False

        # used to know where to ask for a new time-series tablename
        self.last_ps_layerid = None
        self.ts_tablename = None

    def initGui(self):
        # create the actions
        icon_path = os.path.join(os.path.dirname(__file__), 'icons', 'logo.png')
        self.action = QAction( QIcon(icon_path), "PS Time Series Viewer", self.iface.mainWindow() )
        self.action.triggered.connect( self.clickPoint )
        self.action.setCheckable( True )

        icon_path = os.path.join(os.path.dirname(__file__), 'icons', 'about.png')
        self.aboutAction = QAction( QIcon( icon_path ), "About", self.iface.mainWindow() )

        # add actions to toolbars and menus
        self.iface.addToolBarIcon( self.action )
        self.iface.addPluginToMenu( "&Permanent Scatterers", self.action )
        # self.iface.addPluginToMenu( "&Permanent Scatterers", self.aboutAction )

    def unload(self):
        # remove actions from toolbars and menus
        self.iface.removeToolBarIcon( self.action )
        self.iface.removePluginMenu( "&Permanent Scatterers", self.action )
        #self.iface.removePluginMenu( "&Permanent Scatterers", self.aboutAction )

    def clickPoint(self):
        self.canvas = self.iface.mapCanvas()

        ps_layer = self.iface.activeLayer()
        # creates the pointTool: the mouse arrow will change to a pointer
        self.pointTool = QgsMapToolIdentifyFeature(self.canvas, ps_layer)
        # adds the pointer to the canvas
        self.canvas.setMapTool(self.pointTool)
        # when the user clicks on the map then
        self.pointTool.featureIdentified.connect(partial(self._onPointClicked, ps_layer))

    def _onPointClicked(self, ps_layer, feature):

        try:
            x, y, fieldMap, fid, layer = getXYvalues(ps_layer, feature)
        except:
            return
        dlg = createPlotDialog(self.iface, x, y, fieldMap, fid, layer)
        dlg.exec_()
        self.action.setCheckable(False)
        self.canvas.unsetMapTool(self.pointTool)

