# -*- coding: utf-8 -*-

"""
/***************************************************************************
Name                : PS Time Series Viewer
Description         : Computation and visualization of time series of speed for
                    Permanent Scatterers derived from satellite interferometry
Date                : Jul 25, 2012
copyright           : (C) 2012 by Giuseppe Sucameli and Matteo Ghetta (Faunalia)
email               : brush.tyler@gmail.com matteo.ghetta@faunalia.it

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.PyQt import uic
from qgis.PyQt.QtCore import QDate, QCoreApplication
from qgis.PyQt.QtWidgets import QDialog, QVBoxLayout, QCheckBox, QMessageBox
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib
from matplotlib.dates import date2num

from functools import partial

import numpy as np
from statistics import mean
import random

import os

from qgis.core import (
    QgsFeatureRequest,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsPropertyDefinition,
    QgsProperty,
    QgsProject,
    QgsSettings,
    QgsApplication,
    QgsExpression,
    Qgis
)

from qgis.gui import (
    QgsColorButton,
    QgsPropertyOverrideButton
)

from qgis.PyQt.QtGui import QColor, QIcon
from qgis.PyQt.QtCore import pyqtSignal

from ..utils import getXYvalues, CustomRectangleMapTool
from collections import defaultdict


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)
        super(MplCanvas, self).__init__(self.fig)


path = os.path.join(os.path.dirname(__file__), 'create_plot_dialog.ui')

FORM_CLASS, _ = uic.loadUiType(path)

class createPlotDialog(QDialog, FORM_CLASS):

    plot_created = pyqtSignal()

    def __init__(self, iface, x, y, fieldMap, fid, layer, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.iface = iface
        # store the x and y values
        self.x = x
        self.y = y
        self.fieldMap = fieldMap
        self.fid = fid
        self.layer = layer

        self.plots = defaultdict(lambda: defaultdict(dict))
        self.plots[self.fid]['values']['x'] = self.x
        self.plots[self.fid]['values']['y'] = self.y

        # get the X min and max values
        min_x = min(self.x)
        max_x = max(self.x)

        # get the Y min and max values
        min_y = min(self.y)
        max_y = max(self.y)

        # connect the plot default signal to the method
        self.plot_created.connect(self.enableMultiplePlotChoice)

        # connect the radio buttons to the method to enable the error bar
        self.singlePlotRadio.clicked.connect(self.enableErrorBarChoice)
        self.multiplePlotRadio.clicked.connect(self.enableErrorBarChoice)

        # set the X datetime limits
        self.minDateEdit.setDate(QDate(min_x.year, min_x.month, min_x.day))
        self.maxDateEdit.setDate(QDate(max_x.year, max_x.month, max_x.day))

        # set the Y limits
        self.minYEdit.setValue(min_y)
        self.maxYEdit.setValue(max_y)

        # create the map canvas and the toolbar
        self.plot_canvas = MplCanvas(self)
        toolbar = NavigationToolbar(self.plot_canvas, self)

        # add the map canvas and toolbar to the layout
        layout = QVBoxLayout(self.widget)
        layout.addWidget(self.plot_canvas)
        layout.addWidget(toolbar)
        self.colorButton.setColor(QColor(0,0,0))

        self.addPlotButton.clicked.connect(self.clickPoint)
        self.clearButton.clicked.connect(self.clearPlot)
        self.removePlot.clicked.connect(self.removeSelectedPlot)

        # call the method to fill the title comboboxes
        self.fillComboboxes()

        # connect the widgets to the update methods (follow the UI schema)

        # Scale option
        self.minDateEdit.dateChanged.connect(self.updateLimits)
        self.maxDateEdit.dateChanged.connect(self.updateLimits)
        self.minYEdit.valueChanged.connect(self.updateLimits)
        self.maxYEdit.valueChanged.connect(self.updateLimits)

        # Plot option
        self.colorButton.colorChanged.connect(self.setPlotFaceColor)
        self.singlePlotRadio.clicked.connect(self.createMultiplePlot)
        self.multiplePlotRadio.clicked.connect(self.createMultiplePlot)

        # Replica
        self.replicaUpCheck.clicked.connect(self.setReplicas)
        self.replicaDistEdit.valueChanged.connect(self.setReplicas)
        self.replicaDownCheck.clicked.connect(self.setReplicas)

        # Chart options
        self.hGridCheck.clicked.connect(self.updateGrids)
        self.vGridCheck.clicked.connect(self.updateGrids)
        self.linesCheck.clicked.connect(self.createLinePlot)
        self.labelsCheck.stateChanged.connect(self.hideLabels)
        self.linRegrCheck.clicked.connect(self.createLineRegTrendPlot)
        self.polyRegrCheck.clicked.connect(self.createLinePolyregTrendPlot)
        self.detrendingCheck.clicked.connect(self.createDetrendedPlot)
        self.errorbarCheck.clicked.connect(self.createErrorbarPlot)

        # disable the errorbar check at firts run
        self.errorbarCheck.setEnabled(False)

        # Chart axis labels
        self.xLabelEdit.textChanged.connect(self.updateLabels)
        self.yLabelEdit.textChanged.connect(self.updateLabels)

        # Chart title
        self.titleParam0Edit.textChanged.connect(self.updateTitle)
        self.titleParam1Edit.textChanged.connect(self.updateTitle)
        self.titleParam2Edit.textChanged.connect(self.updateTitle)
        self.titleParam0Combo.currentTextChanged.connect(self.updateTitle)
        self.titleParam1Combo.currentIndexChanged.connect(self.updateTitle)
        self.titleParam2Combo.currentIndexChanged.connect(self.updateTitle)

        # call the create plot function
        self.createPlot(self.fid)

        # calling the refresh method for the legend AFTER the plot creation
        self.legendCheck.clicked.connect(self.refresh)

        # call the methods to update the title and the labels
        self.updateTitle()
        self.updateLabels()

        self.plotListCombo.currentIndexChanged.connect(self.setDialogWidgetsState)

        self.definition = QgsPropertyDefinition("PS Time Series", "Field Chosen", QgsPropertyDefinition.DoublePositive)
        self.propertyButton.init(0, QgsProperty(), self.definition, layer, False)
        self.propertyButton.changed.connect(partial(self.setDataDefinedColor, self.layer))

        self.expressionCombo.setLayer(layer)
        self.updatePlotButton.clicked.connect(partial(self.createDoubleYPlot, layer))
        self.updatePlotButton.setIcon(QIcon(QgsApplication.getThemeIcon("mActionRefresh.svg")))
        self.removePlotButton.clicked.connect(self.removeDoubleYPlot)
        self.removePlotButton.setIcon(QIcon(QgsApplication.getThemeIcon("mActionRemove.svg")))

    def createDoubleYPlot(self, layer):
        fid_plot = self.plotListCombo.currentData()

        context = QgsExpressionContext(QgsExpressionContextUtils.globalProjectLayerScopes(layer))

        expression = QgsExpression(self.expressionCombo.asExpression())
        if not expression.isValid():
            self.message_bar.pushMessage('PS Time Series', self.tr('The expresion is no valid'), Qgis.Critical)
            return

        secondary_y_values = []

        for feature in layer.getFeatures():
            context.setFeature(feature)
            value = expression.evaluate(context)
            secondary_y_values.append(value)

        if self.plots[fid_plot]['plots'].get('double_y_plot'):
            self.removeDoubleYPlot()
        try:
            self.double_y_plot = self.twin.scatter(self.x, secondary_y_values, color='green')
            self.twin = self.plot_canvas.axes.twinx()
            self.twin.tick_params(axis='y', labelcolor='green')
            self.plots[fid_plot]['plots']['double_y_plot'] = self.double_y_plot
        except:
            self.message_bar.pushMessage('PS Time Series', self.tr('Double Y Axis not supported'), Qgis.Critical)
            return

        self.refresh()

    def removeDoubleYPlot(self):
        plot_fid = self.getCurrentPlotFid()
        if self.plots[plot_fid]['plots'].get('double_y_plot'):
            self.plots[plot_fid]['plots']['double_y_plot'].axes.set_yticks([])
            self.plots[plot_fid]['plots']['double_y_plot'].remove()
            self.plots[plot_fid]['plots'].pop('double_y_plot')

        self.refresh()

    def setDataDefinedColor(self, layer):
        """
        set the datadefined color (if any field or expression is entered)
        """

        # get the current fid from the combobox
        fid_plot = self.plotListCombo.currentData()

        if self.propertyButton.isActive():
            self.colorButton.setEnabled(False)
            context = QgsExpressionContext(QgsExpressionContextUtils.globalProjectLayerScopes(layer))
            color_value = []

            for feature in layer.getFeatures():
                context.setFeature(feature)
                value, ok = self.propertyButton.toProperty().value(context)
                if ok:
                    color_value.append(value)

            try:
                self.plots[fid_plot]['plots']['scatter'].set_facecolor(matplotlib.cm.viridis(color_value))
                self.plots[fid_plot]['gui']['propertyButton'] = self.propertyButton.toProperty()
            except TypeError as e:
                self.propertyButton.setActive(False)
                self.colorButton.setEnabled(True)
                return

        else:
            self.colorButton.setEnabled(True)
            color = self.colorButton.color().name()
            self.plots[fid_plot]['gui']['propertyButton'] = False

            # update the scatterplot
            self.plots[fid_plot]['plots']['scatter'].set_facecolor(color)
        self.refresh()

    def getCurrentPlotFid(self):
        """
        utility to get the current fid from the combobox
        """
        plot_fid = self.plotListCombo.currentData()
        return plot_fid

    def setDialogWidgetsState(self):
        """
        set the widgets according to the specification of the layer chosen
        """
        plot_fid = self.getCurrentPlotFid()
        widgets = self.findChildren((QCheckBox, QgsColorButton, QgsPropertyOverrideButton))

        # update the widget state according to the dictionary
        for widget in widgets:
            if widget.objectName() in self.plots[plot_fid]['gui'].keys():
                if isinstance(widget, QCheckBox):
                    widget.setChecked(self.plots[plot_fid]['gui'].get(widget.objectName()))
                if isinstance(widget, QgsPropertyOverrideButton):
                    if self.plots[plot_fid]['gui'].get(widget.objectName()):
                        widget.setToProperty(self.plots[plot_fid]['gui'].get(widget.objectName()))
                        self.colorButton.setEnabled(False)
                if isinstance(widget, QgsColorButton):
                    if not self.plots[plot_fid]['gui'].get('propertyButton'):
                        self.colorButton.setEnabled(True)
                        self.propertyButton.setActive(False)
                        widget.setColor(self.plots[plot_fid]['gui'].get(widget.objectName()))

        self.refresh()

    def createMultiplePlot(self):
        """
        function to create a single mean values multiple plot.
        This function takes into account the radio button and behave accordingly
        """

        if self.multiplePlotRadio.isChecked():
            for k, v in self.plots.items():
                if k == 'average':
                    continue
                for plots in v['plots'].values():
                    plots.remove()

            xnew = []
            ynew = []

            for k, v in self.plots.items():
                if k == 'average':
                    continue
                tl_x = self.plots[k]['values']['x']
                tl_y = self.plots[k]['values']['y']

                xnew.append(date2num(np.array(tl_x)))
                ynew.append(np.array(tl_y))

            x = np.mean(xnew, axis=0)
            y = np.mean(ynew, axis=0)

            self.plots['average']['values']['x'] = x
            self.plots['average']['values']['y'] = y

            self.multiple_line_plot = self.plot_canvas.axes.plot(x, y, color='black')
            self.plots['average']['plots']['lines'] = self.multiple_line_plot[0]

        if self.singlePlotRadio.isChecked():
            if self.plots['average']['plots'].get('lines'):
                self.plots['average']['plots']['lines'].remove()
                self.plots.pop('average')
            for k, v in self.plots.items():
                if not k=='average':
                    for plots in v['plots'].values():
                        self.plot_canvas.axes.add_artist(plots)

        self.updateTitle()

    def createErrorbarPlot(self):

        # get x and y from the dictionary
        x = self.plots['average']['values']['x']
        y = self.plots['average']['values']['y']

        # get the list of y values
        list_y = [self.plots[key]['values']['y'] for key in self.plots]

        # calculate the error as absolute value of the mean between each values
        error = [abs(sum(values)/len(values)) for values in zip(*list_y)]

        if self.errorbarCheck.isChecked():
            self.errorbar_plot = self.plot_canvas.axes.errorbar(x, y, error, fmt='.', color='blue', capsize=2.5)
            self.plots['average']['plots']['errorbar'] = self.errorbar_plot
        else:
            self.errorbar_plot.remove()
            self.plots['average']['plots'].pop('errorbar')

        self.refresh()


    def enableMultiplePlotChoice(self):
        """
        enable/disable the multiple radiobutton depending on the plot dictionary keys
        """
        if len(self.plots.keys()) > 1 and not 'average' in self.plots.keys():
            self.multiplePlotRadio.setEnabled(True)
        else:
            self.multiplePlotRadio.setEnabled(False)

    def enableErrorBarChoice(self):
        if self.multiplePlotRadio.isChecked():
            self.errorbarCheck.setEnabled(True)
        else:
            if self.plots['average']['plots'].get('errorbar'):
                self.errorbar_plot.remove()
                self.plots['average']['plots'].pop('errorbar')
            self.errorbarCheck.setEnabled(False)
            self.errorbarCheck.setChecked(False)

        self.refresh()


    def createLineTrendPlot(self, d=1):
        """
        calculates the trend values given a certain distance
        """
        xnew = date2num(np.array(self.x))
        y = np.array(self.y)
        p = np.polyfit(xnew, y, d)
        ynew = np.polyval(p, xnew)

        return xnew, ynew

    def createPlot(self, fid, color=None):
        """
        main function that creates the scatter plot
        """

        # get x and y from the dictionary
        x = self.plots[fid]['values']['x']
        y = self.plots[fid]['values']['y']

        # get the color from the buttoni f not None
        if not color:
            color = self.colorButton.color().name()
            self.plots[fid]['gui']['colorButton'] = self.colorButton.color()

        self.scatter = self.plot_canvas.axes.scatter(x, y, marker='s', color=color, label=f'{fid}')
        self.plots[fid]['plots']['scatter'] = self.scatter

        # dump into the dictionary the state of the widgets
        self.plots[fid]['gui']['linesCheck'] = self.linesCheck.isChecked()
        self.plots[fid]['gui']['linRegrCheck'] = self.linRegrCheck.isChecked()
        self.plots[fid]['gui']['polyRegrCheck'] = self.polyRegrCheck.isChecked()
        self.plots[fid]['gui']['detrendingCheck'] = self.detrendingCheck.isChecked()

        self.refresh()

        # add the fid value to the combobox
        self.plotListCombo.addItem(f'{self.iface.activeLayer().name()}-{fid}', fid)
        self.plotListCombo.setCurrentIndex(self.plotListCombo.count() - 1)

        # emit the plot created signal
        self.plot_created.emit()

    def createLinePlot(self):
        """
        function to create the line plot linking each point of the scatter plot
        """
        # get the plot fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        # get x and y from the dictionary
        x = self.plots[plot_fid]['values']['x']
        y = self.plots[plot_fid]['values']['y']

        if self.linesCheck.isChecked():
            self.line_plot = self.plot_canvas.axes.plot(x, y, color='black')
            self.plots[plot_fid]['plots']['lines'] = self.line_plot[0]

        else:
            if self.plots[plot_fid]['plots'].get('lines'):
                self.line_plot[0].remove()
                self.plots[plot_fid]['plots'].pop('lines')

        self.plots[plot_fid]['gui']['linesCheck'] = self.linesCheck.isChecked()

        self.refresh()

    def setReplicas(self):
        """
        function to add the replicas (up or down) with the value of the spinbox
        """
        # get the plot fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        # get the distance from the spinbox
        dist = self.replicaDistEdit.value()

        # clear the plot dictionary if the dict value exist and clean the plot canvas
        if self.plots[plot_fid]['plots'].get('up_replicas'):
            self.plots[plot_fid]['plots']['up_replicas'].remove()
            self.plots[plot_fid]['plots'].pop('up_replicas')
            self.plots[plot_fid]['values'].pop('y_up')

        if self.plots[plot_fid]['plots'].get('down_replicas'):
            self.plots[plot_fid]['plots']['down_replicas'].remove()
            self.plots[plot_fid]['plots'].pop('down_replicas')
            self.plots[plot_fid]['values'].pop('y_down')

        # create the plots depending on the check made
        if self.replicaUpCheck.isChecked():
            y = list(map(lambda v: v+dist, self.y))
            self.up_trend_plot = self.plot_canvas.axes.scatter(self.x, y, marker='s', color='blue')
            self.plots[plot_fid]['plots']['up_replicas'] = self.up_trend_plot
            self.plots[plot_fid]['values']['y_up'] = y

        if self.replicaDownCheck.isChecked():
            y = list(map(lambda v: v-dist, self.y))
            self.down_trend_plot = self.plot_canvas.axes.scatter(self.x, y, marker='s', color='blue')
            self.plots[plot_fid]['plots']['down_replicas'] = self.down_trend_plot
            self.plots[plot_fid]['values']['y_down'] = y


        self.plots[plot_fid]['gui']['replicaUpCheck'] = self.replicaUpCheck.isChecked()
        self.plots[plot_fid]['gui']['replicaDownCheck'] = self.replicaDownCheck.isChecked()

        # call the updateLimits method to get the correct ymin and ymax to set to the plot
        self.updateLimits()

        self.refresh()

    def createLineRegTrendPlot(self):
        """
        function to create the linear trend line by calling the createLineTrendPlot with
        the d=1 parameter
        """
        if self.linRegrCheck.isChecked():
            xnew, ynew = self.createLineTrendPlot(1)
            self.trend_line_plot = self.plot_canvas.axes.plot(xnew, ynew, color='red')
            self.plots[self.fid]['plots']['line_trend'] = self.trend_line_plot[0]
        else:
            self.plots[self.fid]['plots']['line_trend'].remove()
            self.plots[self.fid]['plots'].pop('line_trend')

        self.refresh()

    def createLinePolyregTrendPlot(self):
        """
        function to create the polynomial trend line by calling the createLineTrendPlot with
        the d=3 parameter
        """
        if self.polyRegrCheck.isChecked():
            xnew, ynew = self.createLineTrendPlot(3)
            self.poly_trend_line_plot = self.plot_canvas.axes.plot(xnew, ynew, color='red')
            self.plots[self.fid]['plots']['poly_trend'] = self.poly_trend_line_plot[0]
        else:
            self.plots[self.fid]['plots']['poly_trend'].remove()
            self.plots[self.fid]['plots'].pop('poly_trend')

        self.refresh()

    def createDetrendedPlot(self):
        """
        function to create the detrended scatter plot. More tricky compare to the other plot
        functions because we need to remove the original plot id the detrended one is chosen
        """

        # clear the plot dictionary if the dict value exist and clean the plot canvas
        if self.plots[self.fid]['plots'].get('detrended'):
            self.plots[self.fid]['plots']['detrended'].remove()
            self.plots[self.fid]['plots'].pop('detrended')

        # get the color from the button
        color = self.colorButton.color().name()

        # if detrended then the original scatterplot should be removed!
        if self.detrendingCheck.isChecked():
            if self.plots[self.fid]['plots'].get('scatter'):
                self.plots[self.fid]['plots']['scatter'].remove()
            y = np.array(self.y) - np.array(self.createLineTrendPlot(1)[1])
            self.scatter_detrended = self.plot_canvas.axes.scatter(self.x, y, marker='s', color=color, label=f'Detrended {self.fid}')
            self.plots[self.fid]['plots']['detrended'] = self.scatter_detrended
        else:
            self.createPlot(self.fid)

        self.refresh()

    def removeSelectedPlot(self):
        """
        remove the plot chosen from the combobox
        """
        fid_plot = self.plotListCombo.currentData()

        for k, v in self.plots[fid_plot]['plots'].items():
            v.remove()
            # remove the plot key from the dictionary
            self.plots.pop(fid_plot)

        # remove the item from the combobox
        self.plotListCombo.removeItem(self.plotListCombo.currentIndex())

        self.refresh()

        # emit the plot created signal
        self.plot_created.emit()

    def refresh(self):
        """
        refresh the plot canvas and checks if the legend should be visible
        """
        if self.legendCheck.isChecked():
            self.plot_canvas.axes.legend()
        else:
            if self.plot_canvas.axes.get_legend():
                self.plot_canvas.axes.get_legend().remove()

        self.plot_canvas.draw()

    def clearPlot(self):
        """
        clear all the plots at once
        """
        # loop into the main dictionary and remove the plots
        for k, v in self.plots.items():
            for plot in v['plots'].values():
                if plot:
                    plot.remove()

        self.refresh()

    def clickPoint(self):
        """
        function that allows to click on a feature of the active layer
        """
        # get the main map canvas
        self.canvas = self.iface.mapCanvas()

        # get the active layer
        ps_layer = self.iface.activeLayer()

        # create the pointTool to identify a feature of the layer
        self.pointTool = CustomRectangleMapTool(self.canvas, ps_layer)

        # adds the pointTool to the map canvas
        self.canvas.setMapTool(self.pointTool)

        # connect the identified feature signal to the method
        self.pointTool.canvasClicked.connect(partial(self._onPointClicked, ps_layer))

    def _onPointClicked(self, ps_layer, features):
        """
        function performed after the feature identification
        """

        # create empty messagebox
        message_box = QMessageBox()

        # QMessageBox depending on the number of features selected
        if len(features) > 20:
            message_box = QMessageBox.question(
                self,
                'PS Time Series Viewer',
                self.tr(f'You have selected {len(features)} features. Are you sure to proceed?'),
                QMessageBox.Yes | QMessageBox.No, QMessageBox.No
            )

        # proceed only if Yes or teh feature count is less than 20
        if message_box == QMessageBox.Yes or len(features) <= 20:

            for feature in features:
                x, y, fieldMap, self.fid, self.structure = getXYvalues(ps_layer, feature)
                # fill the dictionary with the new x and y values
                self.plots[self.fid]['values']['x'] = x
                self.plots[self.fid]['values']['y'] = y

                color = f"#{random.randint(0, 0xFFFFFF):06x}"
                self.plots[self.fid]['gui']['colorButton'] = QColor(color)

                self.createPlot(self.fid, color)

                self.colorButton.setColor(QColor(color))

    def fillComboboxes(self):
        """
        fill the title comboboxes from the fieldMap items
        """
        for k, v in self.fieldMap.items():
            self.titleParam0Combo.addItem(v.name())
            self.titleParam1Combo.addItem(v.name())
            self.titleParam2Combo.addItem(v.name())

    def setPlotFaceColor(self):
        """
        redraw the scatter plot with the chosen color
        """
        # get the color from the button
        color = self.colorButton.color().name()

        # get the current fid from the combobox
        fid_plot = self.plotListCombo.currentData()

        # update the scatterplot
        self.plots[fid_plot]['plots']['scatter'].set_facecolor(color)
        self.plots[fid_plot]['gui']['colorButton'] = self.colorButton.color()
        self.refresh()

    def updateGrids(self):
        """
        show/hide the horizontal or vertical grids
        """
        hgrid = self.hGridCheck.isChecked()
        vgrid = self.vGridCheck.isChecked()

        self.plot_canvas.axes.xaxis.grid(hgrid, 'major')
        self.plot_canvas.axes.yaxis.grid(vgrid, 'major')
        self.refresh()

    def hideLabels(self):
        """
        show/hide the plot x/y labels
        """
        if self.labelsCheck.isChecked():
            self.updateLabels()
        else:
            self.plot_canvas.axes.set_xlabel('')
            self.plot_canvas.axes.set_ylabel('')
            self.refresh()

    def updateLabels(self):
        """
        update the x or y label from the text changed
        """
        # get the text from the widgets
        xtext = self.xLabelEdit.text()
        ytext = self.yLabelEdit.text()

        # update the plot x or y labels
        self.plot_canvas.axes.set_xlabel(xtext)
        self.plot_canvas.axes.set_ylabel(ytext)
        self.refresh()

    def updateLimits(self):
        """
        update the plot limits depending on the widget values
        """
        # get the xmin and xmax values
        x_min = self.minDateEdit.date().toPyDate()
        x_max = self.maxDateEdit.date().toPyDate()

        # get the ymin and ymax values
        y_min = self.minYEdit.value()
        y_max = self.maxYEdit.value()

        # check if the list of y_up replica values exists and overwrite the variable
        if self.plots[self.fid]['values'].get('y_up'):
            y_max = max(self.plots[self.fid]['values'].get('y_up'))
        # check if the list of y_down replica values exists and overwrite the variable
        if self.plots[self.fid]['values'].get('y_down'):
            y_min = min(self.plots[self.fid]['values'].get('y_down'))

        # update the plot limits
        self.plot_canvas.axes.set_xlim(x_min, x_max)
        self.plot_canvas.axes.set_ylim(y_min, y_max)
        self.refresh()

    def updateTitle(self):
        """
        update the plot title from the widgets
        """

        if self.plots.get('average'):
            self.plot_canvas.axes.set_title(f'''{len(self.plots['average']['values']['x'])} features on the plot''')

        else:
            # get the title labels from the widgets
            label_title_param0 = self.titleParam0Edit.text()
            label_title_param1 = self.titleParam1Edit.text()
            label_title_param2 = self.titleParam2Edit.text()

            # get the currentText from the field widgets
            combo_title_param0 = self.titleParam0Combo.currentText()
            combo_title_param1 = self.titleParam1Combo.currentText()
            combo_title_param2 = self.titleParam2Combo.currentText()

            # set up the request to only filter the desired fid and optimize with NoGeometry
            request = QgsFeatureRequest()
            request.setFilterFid(self.fid)
            request.setFlags(QgsFeatureRequest.NoGeometry)

            # loop into the activeLayer and get the correct values
            for feature in self.iface.activeLayer().getFeatures(request):
                val_title0 = feature[f"{combo_title_param0}"]
                val_title1 = feature[f"{combo_title_param1}"]
                val_title2 = feature[f"{combo_title_param2}"]

            # set the title of the plot
            self.plot_canvas.axes.set_title(f'{label_title_param0} {val_title0} {label_title_param1} {val_title1} {label_title_param2} {val_title2}')
        self.refresh()

    def tr(self, message):
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('', message)