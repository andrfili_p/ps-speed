# -*- coding: utf-8 -*-

"""
/***************************************************************************
Name                : PS Time Series Viewer
Description         : Computation and visualization of time series of speed for
                    Permanent Scatterers derived from satellite interferometry
Date                : Jul 25, 2012
copyright           : (C) 2012 by Giuseppe Sucameli and Matteo Ghetta (Faunalia)
email               : brush.tyler@gmail.com matteo.ghetta@faunalia.it

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import json
import psycopg2

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QMessageBox
from processing.algs.gdal.GdalUtils import GdalUtils

from qgis.core import QgsMapLayerProxyModel, QgsDataSourceUri

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'structure_type.ui'))

class DataStructureType(QDialog, FORM_CLASS):
    def __init__(self, iface, parent=None):
        """Constructor"""
        super(DataStructureType, self).__init__(parent)
        self.iface = iface
        self.setupUi(self)
        self.layer_combo.setFilters(QgsMapLayerProxyModel.VectorLayer)

    def writeTable(self, ps_layer):
        '''
        write the settings table in the public schema of the DB
        '''
        # get URI from layer source
        dsuri = QgsDataSourceUri( ps_layer.source() )
        ps_layer_uri = dsuri.uri()
        ps_layer_uri = ps_layer_uri.replace("'", "")

        # if connection with service
        if 'service' in ps_layer_uri:
            for param in dsuri.uri().split(" "):
                if param.startswith('service'):
                    conn = param.split("=")[1]

                    myConnection = psycopg2.connect(service=conn)

        # if connection with user and pws
        else:
            param_dict = {}
            ps_layer_connection_info = GdalUtils.ogrConnectionStringFromLayer(ps_layer)
            ps_layer_connection_info = ps_layer_connection_info.replace("'", "")

            for i in ps_layer_connection_info.split(" "):
                if 'dbname' in i:
                    param_dict['dbname'] = i.split("=")[1]
                if 'host' in i:
                    param_dict['host'] = i.split("=")[1]
                if 'user' in i:
                    param_dict['user'] = i.split("=")[1]
                if 'password' in i:
                    param_dict['password'] = i.split("=")[1]
                if 'port' in i:
                    param_dict['port'] = i.split("=")[1]
            param_dict['schema'] = dsuri.schema()

            myConnection = psycopg2.connect(
                database=param_dict['dbname'],
                user=param_dict['user'],
                password=param_dict['password'],
                host=param_dict['host'],
                port=param_dict['port']
            )

        # start connection, cursor and get a list of available tables in schema
        # startingwith ts_
        with myConnection as mycon:

            structure = None
            if self.int_structure.isChecked():
                structure = self.int_structure.text().lower()
            elif self.ext_structure.isChecked():
                structure = self.ext_structure.text().lower()
            else:
                QMessageBox.information(
                    self.iface.mainWindow(),
                    "PS Time Series Viewer",
                    "Please select a data structure for the selected layer."
                )
                return

            cur = myConnection.cursor()
            query = '''
                CREATE TABLE IF NOT EXISTS public.settings
                (
                  pk serial primary key,
                  name character varying NOT NULL,
                  structure character varying NOT NULL
                )
            '''

            cur.execute(query)

            query_update = f'''INSERT INTO public.settings(name, structure) VALUES ('{dsuri.schema()}.{dsuri.table()}','{structure}')'''

            cur.execute(query_update)

            return structure
